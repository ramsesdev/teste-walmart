'use strict';

myAccount.controller('AdressCtrl', ['$scope','AdressService', function ($scope,AdressService) {
  
  $scope.addresses = AdressService.getAll();
  
  $scope.removeItem = function(index) {
    $scope.addresses.splice(index, 1);
  }

}]);