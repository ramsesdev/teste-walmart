'use strict';

myAccount.controller('OrderListCtrl', ['$scope','OrderService', function ($scope,OrderService) {
    $scope.items = OrderService.getAll();
    
    $scope.removeItem = function(index) {
        $scope.items.splice(index, 1);
    }

    $scope.total = function() {
        var total = 0;
        angular.forEach($scope.items, function(item) {
            total += item.quantity * item.price;
        }) 

        return total;
    }
}]);