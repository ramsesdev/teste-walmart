'use strict';

var myAccount = angular.module('walmartApp', ['ngRoute']);

myAccount.config(function ($routeProvider) {
    $routeProvider
        .when('/pedidos', {
            templateUrl: 'views/orders.html',
            controller: 'NavigationCtrl'
        }).when('/enderecos', {
            templateUrl: 'views/adress.html',
            controller: 'NavigationCtrl'
        }).when('/perfil', {
            templateUrl: 'views/profile.html',
            controller: 'NavigationCtrl'
        })
        .otherwise({
            redirectTo: '/pedidos'
        });
});


myAccount.controller('NavigationCtrl', ['$scope', '$location', function ($scope, $location) {
    $scope.isCurrentPath = function (path) {
      return $location.path() == path;
    };
}]);
