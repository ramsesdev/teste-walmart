'use strict';

myAccount.service('OrderService', function () {
    function getOrder() {
          var items = [{
               "id": 0,
               "isActive": false,
               "price": "851.25",
               "quantity": 1,
               "picture": "http://placehold.it/152x152",
               "name": "Tablet Samsung Galaxy Tab 3 T2100, Android 4.1, Tela 7''"
           }, {
               "id": 1,
               "isActive": false,
               "price": "2436.50",
               "quantity": 1,
               "picture": "http://placehold.it/152x152",
               "name": "Smart TV 3D LED 50'' Sony KDL-50R555A, Full HD, Wi-fi Integrado + 4 Óculos 3D"
           }, {
               "id": 2,
               "isActive": true,
               "price": "93.00",
               "quantity": 5,
               "picture": "http://placehold.it/152x152",
               "name": "Piscina Playground Arco Íris 246 L - Intex"
           }];
            return items;
        }

    return {
        getAll: getOrder
    };
});