'use strict';

myAccount.service('AdressService', function () {
    function getAdress() {
          var adresses = [{
               "id": 0,
               "reciverName": "João Paulo",
               "postalCode": "72110600",
               "adress": "Rua Guerino Giovane Leardini",
               "number": "88",
               "city": "São Paulo",
               "state": "São Paulo",
               "country": "Brasil",
               "type": "Residencial"
           },{
               "id": 0,
               "reciverName": "Pedro",
               "postalCode": "029737040",
               "adress": "Rua Pedro Avancine",
               "number": "33",
               "city": "São Paulo",
               "state": "São Paulo",
               "country": "Brasil",
               "type": "Comercial"
           },{
               "id": 0,
               "reciverName": "Maria",
               "postalCode": "72110600",
               "adress": "Rua augusta",
               "number": "86",
               "city": "São Paulo",
               "state": "São Paulo",
               "country": "Brasil",
               "type": "Outros"
           }];
            return adresses;
        }

    return {
        getAll: getAdress
    };
});